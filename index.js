require("dotenv").config();
const axios = require("axios");
const cors = require("cors");
const express = require("express");

const app = express();
app.use(cors());

function mapDerivative({ url, width, height }) {
  return {
    url,
    width: typeof width === "string" ? parseInt(width) : width,
    height: typeof height === "string" ? parseInt(height) : height,
  };
}

app.get("/albums/:id/", async ({ params: { id: cat_id } }, res) => {
  try {
    const {
      data: {
        result: {
          paging: { total_count, ...paging },
          images,
        },
      },
    } = await axios.get(process.env.PIWIGO_API_URL, {
      params: {
        format: "json",
        method: "pwg.categories.getImages",
        cat_id,
      },
    });
    res.status(200).json({
      paging: { ...paging, total_count: parseInt(total_count) },
      images: images.map(
        ({
          file: title,
          comment: caption,
          element_url: url,
          width,
          height,
          derivatives: {
            thumb,
            "2small": xxsmall,
            xsmall,
            small,
            medium,
            large,
            xlarge,
            xxlarge,
          },
        }) => ({
          title,
          caption,
          url,
          width,
          height,
          derivatives: {
            thumb: mapDerivative(thumb),
            xxsmall: mapDerivative(xxsmall),
            xsmall: mapDerivative(xsmall),
            small: mapDerivative(small),
            medium: mapDerivative(medium),
            large: mapDerivative(large),
            xlarge: mapDerivative(xlarge),
            xxlarge: mapDerivative(xxlarge),
          },
        })
      ),
    });
  } catch ({ response: { status, data } }) {
    res.status(status).json({ data });
  }
});

const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Server is running on ${port} port...`));
